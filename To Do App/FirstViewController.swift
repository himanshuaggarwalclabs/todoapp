//
//  FirstViewController.swift
//  To Do App
//
//  Created by Samar Singla on 20/01/15.
//  Copyright (c) 2015 Gupta&sons. All rights reserved.
//

import UIKit

var ToDoItem :[String] = [] // Global Array


class FirstViewController: UIViewController, UITableViewDelegate {

    
    
    
    @IBOutlet weak var taskTable: UITableView!  // TableView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // it return number of Row or Cell
       return ToDoItem.count
                                                                    // Function Return how many Number of Row in Secion
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell

    {
        // it takes a Cell value in Table
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        
        cell.textLabel?.text = ToDoItem[indexPath.row]
        
        return cell
                                                               // Cell Displaying Function
        
    }
    
override  func viewWillAppear(animated: Bool) {
    
        if var storedtoDoItem: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("todoitem")
        {
             ToDoItem = []
                for var i = 0; i < storedtoDoItem.count ; ++i {
                
                      ToDoItem.append(storedtoDoItem[i] as NSString)
                
                }
        }
    
    // it Reload Data in Table                                             // Cell Adding Function
      taskTable.reloadData()
    
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
        
            ToDoItem.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            
            // For updating Presistent Storage
            
            var fixedtodoitem = ToDoItem
            // For Persistent Storage
            NSUserDefaults.standardUserDefaults().setObject(fixedtodoitem, forKey:"todoitem")
            NSUserDefaults.standardUserDefaults().synchronize()
            
        }
                                                        // Cell Removing function
    }
}


