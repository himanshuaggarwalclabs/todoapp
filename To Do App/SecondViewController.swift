//
//  SecondViewController.swift
//  To Do App
//
//  Created by Samar Singla on 20/01/15.
//  Copyright (c) 2015 Gupta&sons. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController , UITextFieldDelegate {
    
    
    
    @IBOutlet weak var todoItems: UITextField! // TextField
    
    
    @IBAction func addPressed(sender: AnyObject) {
        
        // Append New Item in TododList
        if todoItems.text != "" {
         ToDoItem.append(todoItems.text)
        }
      
       // imutable variable
         var fixedtodoitem = ToDoItem
        
       // For Persistent Storage
         NSUserDefaults.standardUserDefaults().setObject(fixedtodoitem, forKey:"todoitem")
         NSUserDefaults.standardUserDefaults().synchronize()
        
      // For KeyBoard Managing
         self.view.endEditing(true)
        
        
     // It clear the Textfield after
         todoItems.text = ""
                                                                  // Button For Adding value in ToDoTable
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override  func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        self.view.endEditing(true)
        
                                                               // KeyBoard Manging Function
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        todoItems.resignFirstResponder()
        return true
                                                            // KeyBoard Managing Function

    }


}

